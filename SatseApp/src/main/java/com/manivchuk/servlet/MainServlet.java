package com.manivchuk.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class MainServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        out.println("<html><head>");
        out.println("<title>Help Page</title></head><body>");
        out.println("<h2>Submit your information</h2>");
        out.println("<form method=\"post\" action=\"" + req.getContextPath()+
        "/firstsevlet\" >");
        out.println("<table border=\"0\"><tr><td valign=\"top\">");
        out.println("Your first name: </td> <td valign=\"top\">");
        out.println("<input type=\"text\" name=\"firstname\" size=\"20\">");
        out.println("</td></tr><tr><td valign=\top\">");
        out.println("Your last name: </td> <td valign=\"top\">");
        out.println("<input type=\"text\" name=\"lastname\" size=\"20\">");
        out.println("</td></tr><tr><td valign=\"top\">");
        out.println("Yout email: </td> <td valign=\"top\">");
        out.println("<input type=\"text\" name=\"email\" size=\"20\">");
        out.println("</td></tr><tr><td valign=\"top\">");

        out.println("<input type=\"submit\" value=\"Submit info\"></td></tr>");

        out.println("</table></form>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Enumeration paramNames = req.getParameterNames();
        String parName;
        boolean emtyEnum = false;

        if(!paramNames.hasMoreElements())
            emtyEnum = true;

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head>");
        out.println("<title>Submitted Parameters</title></head><body>");

        if(emtyEnum)
            out.println("<h2>Sorry, the request doesn't contain any parameters</h2>");
        else
            out.println("<h2>Here are the submitted parameters values</h2>");
        while (paramNames.hasMoreElements()){
            parName = (String)paramNames.nextElement();
            out.println("<strong>" + parName + "</strong> :" + req.getParameter(parName));
        }
        out.println("</body></html>");
    }
}
