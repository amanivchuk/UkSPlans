package com.manivchuk.servlet.bean;

public class Fps {
    private /*final*/ int id;
    private String callsign;
    private String dep;
    private String eobd;
    private String eobt;
    private String dest;
    private String atd;
    private String eta;

   // public Fps(int id) {
     //   this.id = id;
    //}

    public int getId() {
        return id;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getEobd() {
        return eobd;
    }

    public void setEobd(String eobd) {
        this.eobd = eobd;
    }

    public String getEobt() {
        return eobt;
    }

    public void setEobt(String eobt) {
        this.eobt = eobt;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getAtd() {
        return atd;
    }

    public void setAtd(String atd) {
        this.atd = atd;
    }

    @Override
    public String toString() {
        return "id = " +id + " callsign = " + callsign + " DEP = " + dep
                + " eobd = " + eobd + " eobt = " + eobt + " DEST = " + dest
                + " atd = " + atd.trim() + " eta = " + eta;
    }
}
