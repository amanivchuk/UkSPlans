package com.manivchuk.servlet.bean;

import com.manivchuk.servlet.exception.DBException;

import java.util.*;

public interface FpsDao {
    List<Fps> selectAll() throws DBException;
    int deleteById(int id) throws DBException;
    void insert(Fps fp) throws DBException;
}
