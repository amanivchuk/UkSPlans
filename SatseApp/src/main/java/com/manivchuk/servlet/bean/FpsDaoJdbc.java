package com.manivchuk.servlet.bean;

import com.manivchuk.servlet.exception.DBException;
import com.manivchuk.servlet.exception.DBSystemException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FpsDaoJdbc implements FpsDao {
    private static final String SELECT_ALL_SQL = "SELECT * from fplans.fp";
    public static final String DRIVER_CLASS_NAME="com.mysql.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://127.0.0.1:3306/fplans?autoReconnect=true&useSSL=false";
    private static final String LOGIN = "root";
    private static final String PASSWORD = "root";

    static {
        //JdbcUtils.initDriver(DRIVER_CLASS_NAME);
    }




    public List<Fps> selectAll() throws DBException{
        Connection conn = getConnection();
        Statement statement = null;
        ResultSet rs = null;
        List<Fps> result = new ArrayList<Fps>();
        try{
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            statement = conn.createStatement();
            rs = statement.executeQuery(SELECT_ALL_SQL);
            while(rs.next()){
                //int id = rs.getInt("id");
                String callsign = rs.getString("callsign");
                String dep = rs.getString("dep");
                String eobd = rs.getString("eobd");
                String eobt = rs.getString("eobt");
                String dest = rs.getString("dest");
                String eta = rs.getString("eta");
                Fps fp = new Fps();
                fp.setCallsign(callsign);
                fp.setDest(dest);
                fp.setEobd(eobd);
                fp.setEobt(eobt);
                fp.setDest(dest);
                fp.setEta(eta);
                result.add(fp);

                conn.commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                conn.close();
                if (statement != null) {
                    statement.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Connection getConnection() throws DBSystemException {
        try {
            return DriverManager.getConnection(JDBC_URL, LOGIN, PASSWORD);
        } catch (SQLException e) {
            throw new DBSystemException("Can't create connection!", e);
        }
    }

    public int deleteById(int id) throws DBException{
        return 0;
    }

    public void insert(Fps fp) throws DBException{
        String s = "INSERT INTO fplans.fp (callsign, dep, eobd, eobt, dest, eta) VALUES('" + fp.getCallsign() + "', '" + fp.getDep() + "', '" + fp.getEobd() + "', '" + fp.getEobt() + "', '" + fp.getDest() + "', '" + fp.getEta() + "')";
        //System.out.println("------>>>>>> " + s);
        Connection conn = getConnection();
        Statement statement = null;
        try{
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            statement = conn.createStatement();
            statement.executeUpdate(s);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}
