package com.manivchuk.servlet.bean;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ReadFpFrom {
    private static Set<Fps> allListFp = new HashSet<>();

    public Set<Fps> listFp(){
        return allListFp;
    }

    public void runToWrite() {
        while(true) {
            if (existNewFileinDirectory())
                readDirectory();
            else try {
                System.out.println("No new file in directory. I'm sleeping!");
                TimeUnit.SECONDS.sleep(30);
            } catch (InterruptedException e) {
                System.out.println("Ups, error in run!");
            }
        }
    }


    public boolean existNewFileinDirectory(){
        File f = new File("E:\\Plans");
        File[] fList = f.listFiles();
        if(fList.length != 0)
            return true;
        else
            return false;
    }

    private void readDirectory() {
        File f = new File("E:\\Plans");
        File[] fList = f.listFiles();
        for (int i = 0; i < fList.length; i++) {
            if (fList[i].isFile()) {
                try {
                    List<String> listFp = read(fList[i].getAbsolutePath());
                    if(!listFp.isEmpty()){
                        System.out.println("File was delete = " + fList[i].delete());

                        dataForCreateBean(listFp);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<String> read(String fileName) throws IOException {
        List<String> listFp = new ArrayList<>();
        exists(fileName);
        File file = new File(fileName);
        try {
            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try {
                String s;
                StringBuilder sb = new StringBuilder();
                while ((s = in.readLine()) != null) {
                    if (s.contains(" ********* ")) {
                        String[] line = s.replace(" ********* ", "").split("_");
                        for(String str : line) {
                            sb.append(str);
                            sb.append(" ");
                        }
                    }
                    else if((s.contains("DEP/"))&&(s.length() == 21)){
                        String[] line = s.replace(s.substring(0,13), "").split(":");
                        for(String str : line){
                            sb.append(str);
                        }
                        sb.append(" ");
                    }
                    else if(s.contains("EFS_ATA")){
                        String[] line = s.replace("EFS_ATA : ", "").split("\n");
                        for(String str : line){
                            sb.append(str);
                            sb.append(" ");
                        }
                        sb.append(" ");
                    }
                    else if(s.contains("FP TERMINATED")){
                        sb.append("\n");
                        listFp.add(sb.toString());
                        sb.delete(0,sb.length());

                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return listFp;

    }

    private void exists(String file) throws FileNotFoundException {
        File fileName = new File(file);
        if(!fileName.exists())
            throw new FileNotFoundException(fileName.getName());

    }

    public void dataForCreateBean(List<String> listFp){
        for(int i = 0; i < listFp.size()-1;i++){
            String[] mas = listFp.get(i).split(" ");
            ArrayList<String> oneFp = new ArrayList<>(Arrays.asList(mas));
            createBean(oneFp);
        }
    }

    public void createBean(List<String> listFp){
        if(!listFp.isEmpty()) {
            Fps fp = new Fps();
            fp.setCallsign((!listFp.get(0).isEmpty()) ? listFp.get(0) : "zzzz");
            fp.setDep((!listFp.get(1).isEmpty()) ? listFp.get(1) : "zzzz");
            fp.setEobd((!listFp.get(2).isEmpty()) ? listFp.get(2) : "zzzz");
            fp.setEobt((!listFp.get(3).isEmpty()) ? listFp.get(3) : "zzzz");
            fp.setDest((!listFp.get(4).isEmpty()) ? listFp.get(4) : "zzzz");
            fp.setAtd((!listFp.get(5).endsWith("   ")) ? listFp.get(5) : "zzzz");
            //fp.setEta((!listFp.get(6).endsWith("   ")) ? listFp.get(6) : "zz");

            System.out.println(fp.toString());
            allListFp.add(fp);
        }
    }
}
